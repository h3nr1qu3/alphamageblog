<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        if(!$this->session->userdata('logado')){
            redirect(base_url('admin'));
        }

        $this->load->model('Categorias_model', 'cat');
        # $this->load->model('Usuarios_model','user');
        $this->categorias = $this->cat->listar();
    }

    public function index() {
        $this->load->library('table');

        $data['categorias'] = $this->categorias;

        $data['titulo'] = "Painel Administrativo";
        $data['subtitulo'] = "Categoria";

        $this->load->view('backend/template/html-header', $data);
        $this->load->view('backend/template/template');
        $this->load->view('backend/categoria');
        $this->load->view('backend/template/html-footer');
    }

    public function inserir() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('txtCategoria', 'Nome da categoria', 'required|min_length[3]|is_unique[categoria.titulo]');

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $titulo = $this->input->post('txtCategoria');
            if ($this->cat->adicionar($titulo)) {
                redirect(base_url('admin/categoria'));
            } else {
                echo 'Erro ao inserir categoria';
            }
        }
    }

    public function excluir($idCriptografado) {
        if ($this->cat->excluir($idCriptografado)) {
            redirect(base_url('admin/categoria'));
        } else {
            echo 'Erro ao excluir categoria';
        }
        echo md5($idCriptografado, TRUE);
    }

    public function alterar($idCriptografado) {
        $this->load->library('table');

        $data['categoria'] = $this->cat->listarCategoria($idCriptografado);

        $data['titulo'] = "Painel Administrativo";
        $data['subtitulo'] = "Alterar categoria";

        $this->load->view('backend/template/html-header', $data);
        $this->load->view('backend/template/template');
        $this->load->view('backend/alterar-categoria',$data);
        $this->load->view('backend/template/html-footer');
    }
    
    public function salvarAlteracao($idCrip) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txtCategoria', 'Nome da categoria', 'required|min_length[3]|is_unique[categoria.titulo]');

        if ($this->form_validation->run() == FALSE) {
            $this->alterar($idCrip);
        } else {
            $titulo = $this->input->post('txtCategoria');
            $id = $this->input->post('txtId');
            if ($this->cat->salvarAlteracoes($id,$titulo)) {
                redirect(base_url('admin/categoria'));
            } else {
                echo 'Erro ao inserir categoria';
            }
        }
    }

}
