<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if(!$this->session->userdata('logado')){
            redirect(base_url('admin/entrar'));
        }
    }

    public function index() {
        
        $data['titulo']     = "Painel Administrativo";
        $data['subtitulo']  = "Home";
        
        $this->load->view('backend/template/html-header',$data);
        $this->load->view('backend/template/template');
        $this->load->view('backend/home');
        $this->load->view('backend/template/html-footer');
    }

}
