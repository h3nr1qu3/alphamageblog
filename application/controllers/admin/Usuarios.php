<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Usuarios_model', 'user');
    }

    public function index() {
        if ($this->session->userdata('logado')) {
            $this->controle();
        } else {
            $data['titulo'] = "Painel Administrativo";
            $data['subtitulo'] = "Entrar no sistema";
            $this->load->view('backend/template/html-header', $data);
            $this->load->view('backend/login');
            $this->load->view('backend/template/html-footer');
        }
    }

    public function login() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txtLogin', 'Login', 'required');
        $this->form_validation->set_rules('txtSenha', 'Senha', 'required');

        if ($this->form_validation->run() == FALSE) {
            //redirect(base_url('admin/usuarios'));
            $this->index();
        } else {

            $user = $this->input->post('txtLogin');
            $senha = $this->input->post('txtSenha');
            $this->db->where('user', $user);
            $this->db->where('senha', md5($senha));

            $userDatabase = $this->db->get('usuario')->result();

            if (count($userDatabase) == 1) {

                $sessionData['userLogado'] = $userDatabase[0];
                $sessionData['logado'] = TRUE;

                $this->session->set_userdata($sessionData);

                redirect(base_url('admin'));
            } else {
                $sessionData['userLogado'] = NULL;
                $sessionData['logado'] = FALSE;

                $this->session->set_userdata($sessionData);

                $this->index();
            }
        }
    }

    public function sair() {
        $sessionData['userLogado'] = NULL;
        $sessionData['logado'] = FALSE;

        $this->session->set_userdata($sessionData);

        $this->index();
    }

    public function controle() {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {

            $this->load->library('table');


            $data['titulo'] = "Painel Administrativo";
            $data['subtitulo'] = "Usuário";
            $data['usuarios'] = $this->user->listar_autores();
            $data['usuarioAtual'] = $this->user->listarUsuario(md5($this->session->userdata['userLogado']->id))[0];
            //print_r($data['usuarioAtual']);
            $this->load->view('backend/template/html-header', $data);
            $this->load->view('backend/template/template');
            $this->load->view('backend/usuarios');
            $this->load->view('backend/template/html-footer');
        }
    }

    public function inserir() {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('txtNome', 'Nome do usuário', 'required|min_length[3]');
            $this->form_validation->set_rules('txtUsername', 'Username', 'required|min_length[3]|is_unique[usuario.user]');
            $this->form_validation->set_rules('txtEmail', 'Email do usuário', 'required|valid_email|is_unique[usuario.email]');
            $this->form_validation->set_rules('txtSenha', 'Senha do usuário', 'required|min_length[3]');
            $this->form_validation->set_rules('txtConfirmacao', 'Confirmação de senha', 'required|matches[txtSenha]');
            $this->form_validation->set_rules('txtResumo', 'Resumo do usuário', 'required|min_length[30]');
            if ($this->form_validation->run() == FALSE) {
                $this->controle();
            } else {
                $nome = $this->input->post('txtNome');
                $email = $this->input->post('txtEmail');
                $senha = $this->input->post('txtSenha');
                $resumo = $this->input->post('txtResumo');
                $username = $this->input->post('txtUsername');

                if ($this->user->adicionar($nome, $email, $senha, $username, $resumo)) {
                    $this->controle();
                } else {
                    echo 'Erro ao inserir categoria';
                }
            }
        }
    }

    public function excluir($idCriptografado) {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {
            if ($this->user->excluir($idCriptografado)) {
                $this->controle();
            } else {
                echo 'Erro ao excluir categoria';
            }
            echo md5($idCriptografado, TRUE);
        }
    }

    public function alterar($idCriptografado) {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {

            $data['usuario'] = $this->user->listarUsuario($idCriptografado);

            $data['titulo'] = "Painel Administrativo";
            $data['subtitulo'] = "Alterar usuario";

            $this->load->view('backend/template/html-header', $data);
            $this->load->view('backend/template/template');
            $this->load->view('backend/alterar-usuario', $data);
            $this->load->view('backend/template/html-footer');
        }
    }

    public function salvarAlteracao($idCrip) {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('txtNome', 'Nome do usuário', 'required|min_length[3]');
            $this->form_validation->set_rules('txtUsername', 'Username', 'required|min_length[3]');
            $this->form_validation->set_rules('txtEmail', 'Email do usuário', 'required|valid_email');
            $this->form_validation->set_rules('txtSenha', 'Senha do usuário', 'required|min_length[3]');
            $this->form_validation->set_rules('txtConfirmacao', 'Confirmação de senha', 'required|matches[txtSenha]');
            $this->form_validation->set_rules('txtResumo', 'Resumo do usuário', 'required|min_length[20]');

            if ($this->form_validation->run() == FALSE) {
                $this->alterar($idCrip);
            } else {
                $nome = $this->input->post('txtNome');
                $email = $this->input->post('txtEmail');
                $senha = $this->input->post('txtSenha');
                $resumo = $this->input->post('txtResumo');
                $username = $this->input->post('txtUsername');
                $id = $this->input->post('txtId');
                if ($this->user->salvarAlteracoes($id, $nome, $email, $senha, $username, $resumo)) {
                    $this->controle();
                } else {
                    echo 'Erro ao saltar alterações';
                }
            }
        }
    }

    public function novaFoto() {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {
            $id = $this->input->post('id');

            $config = ['upload_path' => './assets/imgs/usuarios', 'allowed_types' => 'jpg', 'file_name' => $id . '.jpg', 'overwrite' => TRUE];
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                echo $this->upload->display_errors();
            } else {
                $cfg = ['source_image' => './assets/imgs/usuarios/' . $id . '.jpg', 'create_thumb' => FALSE, 'width' => 200, 'height' => 200, 'image_library' => 'gd2'];
                $this->load->library('image_lib', $cfg);
                if ($this->image_lib->resize()) {

                    if ($this->user->salvarFoto($id)) {
                        #redirect(base_url('admin/usuarios/alterar/' . $id));
                        $this->controle();
                    } else {
                        echo 'Erro ao inserir foto';
                    }
                } else {
                    echo $this->image_lib->display_errors();
                }
            }
        }
    }

}
