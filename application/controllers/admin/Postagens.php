<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Postagens extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        if (!$this->session->userdata('logado')) {
            redirect(base_url('admin/entrar'));
        } else {

        $this->load->model('Categorias_model', 'cat');
        $this->load->model('Postagens_model', 'post');
        $this->categorias = $this->cat->listar();
        }
    }

    public function index($pular = null,$post_por_pag = null) {
        $this->load->library('table');
        $this->load->library('pagination');
        
        $post_por_pag = 5;
        $config['base_url']     = base_url("admin/postagens");
        $config['total_rows']   = $this->post->contar();
        $config['per_page']     = $post_por_pag;
        
        $this->pagination->initialize($config); 
        
        $data['links_paginacao'] = $this->pagination->create_links();
        
        $data['titulo'] = "Painel Administrativo";
        $data['subtitulo'] = "Postagens";
        $data['categorias'] = $this->categorias;
        $data['publicacoes'] = $this->post->listarPubPaginada($pular,$post_por_pag);
        $this->load->view('backend/template/html-header', $data);
        $this->load->view('backend/template/template');
        $this->load->view('backend/publicacoes',$data);
        $this->load->view('backend/template/html-footer');
    }
    
    public function inserir() {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('txtTituloPostagem', 'Titulo da publicação','required');
            $this->form_validation->set_rules('txtSubTituloPostagem', 'Subtitulo', 'required');
            $this->form_validation->set_rules('txtTextoPostagem', 'Conteudo', 'required');
            $this->form_validation->set_rules('txtDataPostagem', 'Data', 'required');
            $this->form_validation->set_rules('txtCategoriaPostagem', 'Categoria', 'required');
            
            if ($this->form_validation->run() == FALSE) {
                $this->index();
            } else {
                $titulo = $this->input->post('txtTituloPostagem');
                $subtitulo = $this->input->post('txtSubTituloPostagem');
                $conteudo = $this->input->post('txtTextoPostagem');
                $categoria = $this->input->post('txtCategoriaPostagem');
                $data = $this->input->post('txtDataPostagem');
                $user = $this->input->post('txtId');

                if ($this->post->adicionar($titulo, $subtitulo, $conteudo, $user,$data,$categoria)) {
                    $this->index();
                } else {
                    echo 'Erro ao inserir categoria';
                }
            }
        }
    }

    public function excluir($idCriptografado) {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {
            if ($this->post->excluir($idCriptografado)) {
                $this->index();
            } else {
                echo 'Erro ao excluir categoria';
            }
            
        }
    }

    public function alterar($idCriptografado) {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {

            $data['publicacoes'] = $this->post->listarPublicacao($idCriptografado);
            $data['categorias'] = $this->categorias;

            $data['titulo'] = "Painel Administrativo";
            $data['subtitulo'] = "Alterar postagem";

            $this->load->view('backend/template/html-header', $data);
            $this->load->view('backend/template/template');
            $this->load->view('backend/alterar-publicacao', $data);
            $this->load->view('backend/template/html-footer');
        }
    }

    public function salvarAlteracao($idCrip) {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('txtTitulo',      'Titulo',       'required');
            $this->form_validation->set_rules('txtSubtitulo',   'Subtitulo',    'required');
            $this->form_validation->set_rules('txtCategoria',   'Categoria',    'required');
            $this->form_validation->set_rules('txtConteudo',    'Conteudo',     'required');

            if ($this->form_validation->run() == FALSE) {
                $this->alterar($idCrip);
            } else {
                $titulo = $this->input->post('txtTitulo');
                $subtitulo = $this->input->post('txtSubtitulo');
                $categoria = $this->input->post('txtCategoria');
                $texto  = $this->input->post('txtConteudo');
                $id     = $this->input->post('txtId');
                
                if ($this->post->salvarAlteracoes($id, $titulo,$subtitulo,$texto,$categoria)) {
                    $this->index();
                    //echo $this->post->salvarAlteracoes($id, $titulo,$subtitulo,$texto,$categoria);
                } else {
                    echo 'Erro ao salvar alterações';
                }
            }
        }
    }

    public function novaFoto() {
        if (!$this->session->userdata('logado')) {
            $this->index();
        } else {
            $id = $this->input->post('id');
           
            $config = ['upload_path' => './assets/imgs/publicacoes', 'allowed_types' => 'jpg', 'file_name' => $id . '.jpg', 'overwrite' => TRUE];
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                echo $this->upload->display_errors();
            } else {
                $cfg = ['source_image' => './assets/imgs/publicacoes/' . $id . '.jpg', 'create_thumb' => FALSE, 'width' => 900, 'height' => 300, 'image_library' => 'gd2'];
                $this->load->library('image_lib', $cfg);
                if ($this->image_lib->resize()) {

                    if ($this->post->salvarFoto($id)) {
                        redirect(base_url('admin/postagens/alterar/' . $id));
                        #$this->controle();
                    } else {
                        echo 'Erro ao inserir foto';
                    }
                } else {
                    echo $this->image_lib->display_errors();
                }
            }
        }
    }
}
