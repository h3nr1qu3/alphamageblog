<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class sobrenos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('Categorias_model','cat');
        $this->load->model('Usuarios_model','user');
        $this->categorias   = $this->cat->listar();
    }

    public function index() {
        
        $data['categorias'] = $this->categorias;
        
        $data['autores']  = $this->user->listar_autores();
        
        $data['titulo']     = "Sobre nós";
        $data['subtitulo']  = 'Conheça nosso time !';
        
        $this->load->view('frontend/template/html-header',$data);
        $this->load->view('frontend/template/header');
        $this->load->view('frontend/sobrenos');
        $this->load->view('frontend/template/aside');
        $this->load->view('frontend/template/footer');
        $this->load->view('frontend/template/html-footer');
    }
    public function autor($id,$slug=null){
        $data['autores'] = $this->user->info($id);
        $data['titulo']     = "Sobre nós";
        $data['subtitulo']  = 'Autor';
        $data['categorias']  = $this->categorias;
        
        $this->load->view('frontend/template/html-header',$data);
        $this->load->view('frontend/template/header');
        $this->load->view('frontend/autor',$data);
        $this->load->view('frontend/template/aside');
        $this->load->view('frontend/template/footer');
        $this->load->view('frontend/template/html-footer');
    }
    

}
