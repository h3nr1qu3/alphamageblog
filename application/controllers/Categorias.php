<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Categorias_model', 'cat');
        $this->load->model('Postagens_model', 'post');
        $this->categorias = $this->cat->listar();
    }

    public function index($id, $pular = null, $offset = null) {

        $offset = 5;
        $config['base_url'] = base_url("categorias");
        $config['total_rows'] = $this->post->contar();
        $config['per_page'] = $post_por_pag;

        $this->pagination->initialize($config);

        $this->postagens = $this->post->pub_cat($id, $pular, $offset);
        $data['links_paginacao'] = $this->pagination->create_links();
        $data['categorias'] = $this->categorias;
        $data['postagens'] = $this->postagens;
        $data['titulo'] = "Categorias";
        $data['subtitulodb'] = $this->cat->info($id);
        $data['subtitulo'] = '';

        $this->load->view('frontend/template/html-header', $data);
        $this->load->view('frontend/template/header');
        $this->load->view('frontend/categoria', $data);
        $this->load->view('frontend/template/aside');
        $this->load->view('frontend/template/footer');
        $this->load->view('frontend/template/html-footer');
    }

}
