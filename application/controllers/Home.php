<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('Categorias_model','cat');
        $this->load->model('Postagens_model','post');
        $this->categorias   = $this->cat->listar();
        $this->postagens    = $this->post->destaques();
    }

    public function index() {
        $data['categorias'] = $this->categorias;
        $data['postagens']  = $this->postagens;
        $data['titulo']     = "Página Inicial";
        $data['subtitulo']  = "Postagens recentes";
        $this->load->view('frontend/template/html-header',$data);
        $this->load->view('frontend/template/header');
        $this->load->view('frontend/home',$data);
        $this->load->view('frontend/template/aside');
        $this->load->view('frontend/template/footer');
        $this->load->view('frontend/template/html-footer');
    }

}
