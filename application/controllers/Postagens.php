<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Postagens extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('Categorias_model','cat');
        $this->load->model('Postagens_model','post');
        $this->categorias   = $this->cat->listar();
    }

    public function index($id,$slug = null ) {
        $this->postagens    = $this->post->publicacao($id);
        
        $data['categorias'] = $this->categorias;
        $data['postagens']  = $this->postagens;
        $data['titulo']     = "Publicação";
        $data['subtitulodb']  = $this->post->listar_titulo($id);
        $data['subtitulo']  = '';
        
        $this->load->view('frontend/template/html-header',$data);
        $this->load->view('frontend/template/header');
        $this->load->view('frontend/publicacao',$data);
        $this->load->view('frontend/template/aside');
        $this->load->view('frontend/template/footer');
        $this->load->view('frontend/template/html-footer');
    }

}
