<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

    public $id;
    public $nome;
    public $email;
    public $img;
    public $resumo;
    public $user;
    public $senha;

    public function __construct() {
        parent:: __construct();
    }

    public function info($id) {
        $this->db->select('id,nome,resumo,img');
        $this->db->from('usuario');
        $this->db->where('id = ' . $id);
        return $this->db->get()->result();
    }

    public function listar_autores() {
        $this->db->select('id,nome,img,admin');
        $this->db->from('usuario');
        $this->db->order_by('nome', 'ASC');
        return $this->db->get()->result();
    }

    public function adicionar($nome, $email, $senha, $username, $resumo) {
        return $this->db->insert('usuario', array('nome' => $nome, 'email' =>
                    $email, 'senha' => md5($senha), 'user' => $username, 'resumo' => $resumo));
    }

    public function excluir($id) {
        $this->db->where('md5(id)', $id);
        return $this->db->delete('usuario');
    }

    public function listarUsuario($id) {
        $this->db->from('usuario');
        $this->db->where('md5(id)', $id);
        return $this->db->get()->result();
    }

    public function salvarAlteracoes($id, $nome, $email, $senha, $username, $resumo) {
        $this->db->where('id', $id);
        return $this->db->update('usuario', array('nome' => $nome, 'email' =>
                    $email, 'senha' => md5($senha), 'user' => $username, 'resumo' => $resumo));
    }

    public function salvarFoto($id) {
        $this->db->where('md5(id)', $id);
        return $this->db->update('usuario',array('img'=>1));
    }

}
