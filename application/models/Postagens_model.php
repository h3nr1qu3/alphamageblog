<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Postagens_model extends CI_Model {

    public $id;
    public $titulo;
    public $subtitulo;
    public $conteudo;
    public $categoria;
    public $user;
    public $data;
    public $img;

    public function __construct() {
        parent:: __construct();
    }

    public function listar() {
        $this->db->order_by('data', 'DESC');
        return $this->db->get('postagens')->result();
    }

    public function destaques() {
        $this->db->select('usuario.id as idautor, usuario.nome, postagens.id,'
                . ' postagens.titulo, postagens.user, postagens.subtitulo,'
                . ' postagens.data, postagens.img');
        $this->db->from('postagens');
        $this->db->join('usuario', ' usuario.id = postagens.user');
        $this->db->limit(4);
        $this->db->order_by('postagens.data', 'DESC');
        return $this->db->get()->result();
    }

    public function pub_cat($id) {
        $this->db->select('usuario.id as idautor, usuario.nome, postagens.id,'
                . ' postagens.titulo, postagens.user, postagens.subtitulo,'
                . ' postagens.data, postagens.img, postagens.categoria');
        $this->db->from('postagens');
        $this->db->join('usuario', ' usuario.id = postagens.user');
        $this->db->where('postagens.categoria =' . $id);
        $this->db->order_by('postagens.data', 'DESC');
        return $this->db->get()->result();
    }

    public function publicacao($id) {
        $this->db->select('usuario.id as idautor, usuario.nome, postagens.id,'
                . ' postagens.titulo, postagens.user, postagens.subtitulo,'
                . ' postagens.data, postagens.img, postagens.categoria, postagens.conteudo');
        $this->db->from('postagens');
        $this->db->join('usuario', ' usuario.id = postagens.user');
        $this->db->where('postagens.id =' . $id);
        return $this->db->get()->result();
    }

    public function info($id) {
        $this->db->from('postagens');
        $this->db->where('id = ' . $id);
        return $this->db->get()->result();
    }

    public function listar_titulo($id) {
        $this->db->select('id,titulo');
        $this->db->from('postagens');
        $this->db->where('id = ' . $id);
        return $this->db->get()->result();
    }
    
     public function listarPub() {
        $this->db->order_by('data', 'DESC');
        return $this->db->get('postagens')->result();
    }
    
    public function listarPubPaginada($pular = null,$offset = null) {
        $this->db->order_by('data', 'DESC');
        if($pular && $offset){
            $this->db->limit($offset,$pular);
        }else{
            $this->db->limit(5);
        }
        return $this->db->get('postagens')->result();
    }
    
    public function adicionar($titulo,$subtitulo,$conteudo,$idUser,$data,$categoria) {
        return $this->db->insert('postagens', array('titulo' => $titulo,
            'subtitulo'=>$subtitulo,'conteudo'=>$conteudo,'user'=>$idUser,
                'data'=>$data,'categoria'=>$categoria));
    }

    public function excluir($id) {
        $this->db->where('md5(id)', $id);
        return $this->db->delete('postagens');
    }

    public function listarPublicacao($id) {
        $this->db->from('postagens');
        $this->db->where('md5(id)', $id);
        return $this->db->get()->result();
    }

    public function salvarAlteracoes($id, $titulo,$subtitulo,$texto,$categoria) {
        $this->db->where('id', $id);
        return $this->db->update('postagens', array('titulo' => $titulo,
            'subtitulo'=>$subtitulo,'conteudo'=>$texto,'categoria'=>$categoria));
    }
    public function salvarFoto($id) {
        $this->db->where('md5(id)', $id);
        return $this->db->update('postagens',array('img'=>1));
    }
    public function contar(){
        return $this->db->count_all('postagens');
    }

}
