<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias_model extends CI_Model {

    public $id;
    public $titulo;

    public function __construct() {
        parent:: __construct();
    }

    public function listar() {
        $this->db->order_by('titulo', 'ASC');
        return $this->db->get('categoria')->result();
    }

    public function info($id) {
        $this->db->from('categoria');
        $this->db->where('id = ' . $id);
        return $this->db->get()->result();
    }

    public function adicionar($titulo) {
        return $this->db->insert('categoria', array('titulo' => $titulo));
    }

    public function excluir($id) {
        $this->db->where('md5(id)', $id);
        return $this->db->delete('categoria');
    }

    public function listarCategoria($id) {
        $this->db->from('categoria');
        $this->db->where('md5(id)', $id);
        return $this->db->get()->result();
    }

    public function salvarAlteracoes($id, $titulo) {
        $this->db->where('id', $id);
        return $this->db->update('categoria', array('titulo' => $titulo));
    }

}
