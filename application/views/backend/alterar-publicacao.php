<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $subtitulo ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $subtitulo ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <?php
                            echo validation_errors('<div class="alert alert-danger">', '</div>');
                            foreach ($publicacoes as $info) {
                            echo form_open('admin/postagens/salvarAlteracao/'.md5($info->id));
                                ?>
                                <div class="form-group">
                                    <label id="txtTitulo">Titulo</label>
                                    <input type="text" name="txtTitulo" class="form-control" placeholder="Digite o titulo" value="<?= $info->titulo ?>">

                                </div>
                                <div class="form-group">
                                    <label id="txtSubtitulo">SubTitulo</label>
                                    <input type="text" name="txtSubtitulo" class="form-control" placeholder="Digite o subtitulo" value="<?= $info->subtitulo ?>">
                                </div> 
                                <div class="form-group">
                                    <label id="txtData">Data</label>
                                    <input type="text" name="txtData" class="form-control" placeholder="<?= $info->data?>" disabled="">
                                </div>
                                <div class="form-group">
                                    <label id="txtConteudo">Conteudo</label>
                                    <textarea name="txtConteudo" class="form-control" placeholder="Digite o conteúdo" ><?= $info->conteudo; ?></textarea>
                                </div>
                            
                            <div class="form-group">
                                <label id="txtCategoria">Categoria</label>
                                <select class="form-control" name="txtCategoria">
                                    <?php foreach ($categorias as $categoria) { ?>
                                        <option value="<?= $categoria->id ?>"><?= $categoria->titulo ?></option>
                                    <?php } ?>
                                </select>
                                
                                <input type="hidden" name="txtId" id="txtId" class="form-control" value="<?php echo $info->id ?>">
                                <button type="submit" class="btn btn-default">Alterar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                                <?php
                                echo form_close();
                                ?>


                            </div>


                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>

            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading">Imagem de destaque
                    </div>
                    <div class="panel-body">
                        <div class="row" style="padding-bottom: 10px">
                            <div class="col-lg-3">
                                <?php
                                if ($info->img == 1) {
                                    echo img("./assets/imgs/publicacoes/" . md5($info->id) . '.jpg');
                                } else {
                                    echo img("./assets/imgs/publicacoes/pubSemFoto.png");
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $divO = '<div class="form-group">';
                                $divC = '</div>';

                                echo form_open_multipart('admin/postagens/novaFoto');
                                echo form_hidden('id', md5($info->id));
                                #input type="file" required> 
                                echo $divO;
                                $img = ['name' => 'userfile', 'id' => 'userfile', 'class' => 'form-control'];
                                echo form_upload($img);
                                echo $divC;

                                echo $divO;
                                $bt = ['name' => 'btAdd', 'id' => 'btAdd', 'class' => 'btn btn-default', 'value' => 'Adicionar foto'];
                                echo form_submit($bt);
                                echo $divC;
                                echo form_close();
                            }
                            ?>
                        </div>

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                <!-- /.col-lg-12 -->
                <!-- /.row -->
                <!-- /#page-wrapper -->

                <!--
                <form role="form">
                                                <div class="form-group">
                                                    <label>Titulo</label>
                                                    <input class="form-control" placeholder="Entre com o texto">
                                                </div>
                                                <div class="form-group">
                                                    <label>Foto Destaque</label>
                                                    <input type="file">
                                                </div>
                                                <div class="form-group">
                                                    <label>Conteúdo</label>
                                                    <textarea class="form-control" rows="3"></textarea>
                                                </div>
                                               
                                                <div class="form-group">
                                                    <label>Selects</label>
                                                    <select class="form-control">
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                </div>
                                                <button type="submit" class="btn btn-default">Cadastrar</button>
                                                <button type="reset" class="btn btn-default">Limpar</button>
                                            </form>-->