<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $subtitulo ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $subtitulo ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <?php
                            echo validation_errors('<div class="alert alert-danger">', '</div>');
                            foreach ($usuario as $info) {
                            echo form_open('admin/usuarios/salvarAlteracao/'.md5($info->id));
                                ?>
                                <div class="form-group">
                                    <label id="txtNome">Nome</label>
                                    <input type="text" name="txtNome" class="form-control" placeholder="Digite o nome" value="<?php echo $info->nome; ?>">
                                    <input type="hidden" name="txtId" id="txtId" class="form-control" value="<?php echo $info->id ?>">
                                </div>
                                <div class="form-group">
                                    <label id="txtEmail">Email</label>
                                    <input type="text" name="txtEmail" class="form-control" placeholder="Digite o email" value="<?php echo $info->email; ?>">
                                </div>
                                <div class="form-group">
                                    <label id="txtUsername">Username</label>
                                    <input type="text" name="txtUsername" class="form-control" placeholder="Digite o username" value="<?php echo $info->user; ?>">
                                </div>
                                <div class="form-group">
                                    <label id="txtSenha">Senha</label>
                                    <input type="password" name="txtSenha" class="form-control" placeholder="Digite a senha" value="">
                                </div>
                                <div class="form-group">
                                    <label id="txtConfirmacao">Confirme a senha</label>
                                    <input type="password" name="txtConfirmacao" class="form-control" placeholder="Confirme a senha" value="">
                                </div>
                                <div class="form-group">
                                    <label id="txtResumo">Resumo</label>
                                    <textarea name="txtResumo" class="form-control" placeholder="Digite seu resumo" ><?php echo $info->resumo; ?></textarea>
                                </div>

                                <button type="submit" class="btn btn-default">Alterar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                                <?php
                                echo form_close();
                                ?>


                            </div>

                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>

            <!-- /.panel-body -->

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Imagem de destaque
                    </div>
                    <div class="panel-body">
                        <div class="row" style="padding-bottom: 10px">
                            <div class="col-lg-3 col-lg-offset-3">
                                <?php
                                if ($info->img == 1) {
                                    echo img("./assets/imgs/usuarios/" . md5($info->id) . '.jpg');
                                }else{
                                    echo img("./assets/imgs/usuarios/semFoto.png");
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $divO = '<div class="form-group">';
                                $divC = '</div>';

                                echo form_open_multipart('admin/usuarios/novaFoto');
                                echo form_hidden('id', md5($info->id));
                                #input type="file" required> 
                                echo $divO;
                                $img = ['name' => 'userfile', 'id' => 'userfile', 'class' => 'form-control'];
                                echo form_upload($img);
                                echo $divC;

                                echo $divO;
                                $bt = ['name' => 'btAdd', 'id' => 'btAdd', 'class' => 'btn btn-default', 'value' => 'Adicionar foto'];
                                echo form_submit($bt);
                                echo $divC;
                                echo form_close();
                            }
                            ?>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>

<!-- /.col-lg-12 -->
<!-- /.row -->
<!-- /#page-wrapper -->

<!--
<form role="form">
                                <div class="form-group">
                                    <label>Titulo</label>
                                    <input class="form-control" placeholder="Entre com o texto">
                                </div>
                                <div class="form-group">
                                    <label>Foto Destaque</label>
                                    <input type="file">
                                </div>
                                <div class="form-group">
                                    <label>Conteúdo</label>
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                               
                                <div class="form-group">
                                    <label>Selects</label>
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-default">Cadastrar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                            </form>-->