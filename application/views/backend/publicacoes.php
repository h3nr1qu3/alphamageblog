<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo 'Administrar ' . $subtitulo ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo 'Adicionar ' . $subtitulo ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <?php
                            echo validation_errors('<div class="alert alert-danger">', '</div>');
                            echo form_open('admin/postagens/inserir');
                            ?>
                            <div class="form-group">
                                <label id="txtTituloPostagem">Titulo</label>
                                <input type="text" name="txtTituloPostagem" class="form-control" placeholder="Digite o titulo da postagem"
                                       value="<?php echo set_value('txtTituloPostagem') ?>">
                            </div>

                            <div class="form-group">
                                <label id="txtSubTituloPostagem">Subtitulo</label>
                                <input type="text" name="txtSubTituloPostagem" class="form-control" placeholder="Digite o subtitulo da postagem"
                                       value="<?php echo set_value('txtSubTituloPostagem') ?>">
                            </div>

                            <div class="form-group">
                                <label id="txtDataPostagem">Data</label>
                                <input type="datetime-local" name="txtDataPostagem" class="form-control" placeholder="AAAA-MM-DD HH:MM" value="<?= set_value('txtData') ?>">
                            </div>


                            <div class="form-group">
                                <label id="txtTextoPostagem">Texto</label>
                                <textarea   name="txtTextoPostagem" class="form-control" placeholder="Escreva o texto !">
                                    <?php echo set_value('txtTextoPostagem') ?>
                                </textarea>

                            </div>
                            <div class="form-group">
                                <label id="txtCategoriaPostagem">Categoria</label>
                                <select class="form-control" name="txtCategoriaPostagem">
                                    <?php foreach ($categorias as $categoria) { ?>
                                        <option value="<?= $categoria->id ?>"><?= $categoria->titulo ?></option>
                                    <?php } ?>
                                </select>

                            </div>
                            <input type="hidden" id="txtId" name="txtId" value="<?= $this->session->userdata('userLogado')->id ?>">
                            <button type="submit" class="btn btn-default">Salvar</button>
                            <button type="reset" class="btn btn-default">Limpar</button>
                            <?php
                            echo form_close();
                            ?>


                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo 'Alterar ' . $subtitulo ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <style>
                                img{
                                    width: 90px;
                                }
                            </style>

                            <?php
                            $this->table->set_heading("Foto", "Titulo da publicação", "Data", "Alterar", "Excluir");
                            foreach ($publicacoes as $pub) {
                                if ($pub->img == 1) {
                                    $fotoUser = img("./assets/imgs/publicacoes/" . md5($pub->id) . '.jpg');
                                } else {
                                    $fotoUser = img("./assets/imgs/publicacoes/pubSemFoto.png");
                                }
                                $dataPost = postadoem($pub->data);
                                $alterarPub = anchor(base_url('admin/postagens/alterar/' . md5($pub->id)), '<i class="fa fa-refresh fa-fw"></i>Alterar');
                                $tituloPost = $pub->titulo;
                                $excluirPub = anchor(base_url('admin/postagens/excluir/' . md5($pub->id)), "<i class=fa fa-remove fa-fw></i>Excluir");
                                $this->table->add_row($fotoUser, $tituloPost, $dataPost, $alterarPub, $excluirPub);
                            }

                            $this->table->set_template(array(
                                'table_open' => '<table class="table table-striped">'
                            ));

                            echo $this->table->generate();
                            echo "<div class='paginacao'>".$links_paginacao."</div>"
                            ?>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

<!--
<form role="form">
                                <div class="form-group">
                                    <label>Titulo</label>
                                    <input class="form-control" placeholder="Entre com o texto">
                                </div>
                                <div class="form-group">
                                    <label>Foto Destaque</label>
                                    <input type="file">
                                </div>
                                <div class="form-group">
                                    <label>Conteúdo</label>
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                               
                                <div class="form-group">
                                    <label>Selects</label>
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-default">Cadastrar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                            </form>-->