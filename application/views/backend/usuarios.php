<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo 'Administrar ' . $subtitulo ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php
                    echo 'Adicionar ' . $subtitulo;
                    if ($usuarioAtual->admin != 1) {
                        echo '
                        <div class = "panel-body">
                        <div class = "row">
                        <div class = "col-lg-12">
                            Você não tem permissão para adicionar usuários
                                </div>
                                </div>
                                </div>';
                    } else {
                        ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">

    <?php
    echo validation_errors('<div class="alert alert-danger">', '</div>');
    echo form_open('admin/usuarios/inserir');
    ?>
                                <div class="form-group">
                                    <label id="txtNome">Nome</label>
                                    <input type="text" name="txtNome" class="form-control" placeholder="Digite nome"
                                           value="<?php echo set_value('txtNome') ?>">
                                </div>
                                <div class="form-group">
                                    <label id="txtEmail">Email</label>
                                    <input type="email" name="txtEmail" class="form-control" placeholder="Digite o email"
                                           value="<?php echo set_value('txtEmail') ?>">
                                </div>
                                <!--                            <div class="form-group">
                                                                <label id="txtCategoria">Foto</label>
                                                                <input type="text" name="txtUsuario" class="form-control" placeholder="Digite o nome do usuário">
                                                            </div>-->
                                <div class="form-group">
                                    <label id="txtUsername">Username</label>
                                    <input type="text" name="txtUsername" class="form-control" placeholder="Digite o username"
                                           value="<?php echo set_value('txtUsername') ?>">
                                </div>
                                <div class="form-group">
                                    <label id="txtSenha">Senha</label>
                                    <input type="password" name="txtSenha" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label id="txtConfirmacao">Confirmar senha</label>
                                    <input type="password" name="txtConfirmacao" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label id="txtResumo">Resumo</label>
                                    <textarea   name="txtResumo" class="form-control" placeholder="Escreva um resumo sobre você !">
    <?php echo set_value('txtResumo') ?>
                                    </textarea>

                                </div>
                                <button type="submit" class="btn btn-default">Salvar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
    <?php
    echo form_close();
    ?>


                            </div>


                        </div>
<?php } ?>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
<?php echo 'Alterar ' . $subtitulo ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <style>
                                img{
                                    width: 60px;
                                }
                            </style>

<?php
$this->table->set_heading("Foto", "Nome do usuario", "Alterar", "Excluir");
foreach ($usuarios as $user) {
    if ($user->img == 1) {
        $fotoUser = img("./assets/imgs/usuarios/" . md5($user->id) . '.jpg');
    } else {
        $fotoUser = img("./assets/imgs/usuarios/semFoto.png");
    }

    if ($usuarioAtual->admin != 1) {
        $alterarUser = "Ação negada";
        $excluirUser = "Ação negada";
    } else {
        if ($user->admin == 1 & $user->id != $usuarioAtual->id) {
            $alterarUser = "Ação negada";
            $excluirUser = "Ação negada";
        }else{
        $alterarUser = anchor(base_url('admin/usuarios/alterar/' . md5($user->id)), '<i class="fa fa-refresh fa-fw"></i>Alterar');
        $excluirUser = anchor(base_url('admin/usuarios/excluir/' . md5($user->id)), "<i class=fa fa-remove fa-fw></i>Excluir");
        }
    }

    $nomeUser = $user->nome;
    $this->table->add_row($fotoUser, $nomeUser, $alterarUser, $excluirUser);
}

$this->table->set_template(array(
    'table_open' => '<table class="table table-striped">'
));

echo $this->table->generate();
?>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

<!--
<form role="form">
                                <div class="form-group">
                                    <label>Titulo</label>
                                    <input class="form-control" placeholder="Entre com o texto">
                                </div>
                                <div class="form-group">
                                    <label>Foto Destaque</label>
                                    <input type="file">
                                </div>
                                <div class="form-group">
                                    <label>Conteúdo</label>
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>
                               
                                <div class="form-group">
                                    <label>Selects</label>
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-default">Cadastrar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>
                            </form>-->