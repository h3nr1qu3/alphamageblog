<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                <?php echo $titulo ?>
                <small> > <?php
                    if ($subtitulo != null) {
                        echo $subtitulo;
                    } else {
                        foreach($subtitulodb as $info){
                            echo $info->titulo;
                        }
                    }
                    ?></small>
            </h1>

            <!-- First Blog Post -->
            <?php foreach ($postagens as $post) {
                
                 $imagem;
                if ($post->img == 1) {
                    $imagem = "./assets/imgs/publicacoes/" . md5($post->id) . '.jpg';
                } else {
                    $imagem = "./assets/imgs/publicacoes/pubSemFoto.png";
                }
                
                ?>
                <h2><a href="<?php echo base_url('postagem/' . $post->id . '/' . limpar($post->titulo)) ?>"><?php echo $post->titulo ?></a></h2>
                por <a href="<?php echo base_url('autor/' . $post->idautor . '/' . limpar($post->nome)) ?>"><?php echo $post->nome ?></a>
                <p class="lead">
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Postado em <?php echo postadoem($post->data) ?></p>
                <hr>
                <img class="img-responsive" src="<?= base_url($imagem)?>" alt="">
                <hr>
                <p><?php echo $post->subtitulo ?></p>
                <a class="btn btn-primary" href="<?php echo base_url('postagem/' . $post->id . '/' . limpar($post->titulo)) ?>">Leia mais <span class="glyphicon glyphicon-chevron-right"></span></a>
                <hr>
                <?php
            }
            ?>
        </div>