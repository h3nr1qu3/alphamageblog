<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                <?php echo $titulo ?>
                <small> > <?php
                    if ($subtitulo != null) {
                        echo $subtitulo;
                    } else {
                        foreach ($subtitulodb as $info) {
                            echo $info->titulo;
                        }
                    }
                    ?></small>
            </h1>

            <!-- First Blog Post -->
            <?php foreach ($autores as $autor) {
                ?>
                
                <p class="lead">
                <div class="col-md-4">
                    <?php
                    $imagem;
                    if ($autor->img == 1) {
                                    $imagem = "./assets/imgs/usuarios/" . md5($autor->id) . '.jpg';
                                }else{
                                    $imagem = "./assets/imgs/usuarios/semFoto.png";
                                }
                    ?>
                    <img class="img-responsive img-circle" src="<?= base_url($imagem)?>" alt="">
                </div>
                <div class="col-md-8 ">
                    <h2>
                        <?php echo $autor->nome ?>
                    </h2> 
                    <hr>
                    <p>
                        <?php echo $autor->resumo ?>

                    </p>

                    <hr>
                </div>
                <hr>
                <?php
            }
            ?>
        </div>