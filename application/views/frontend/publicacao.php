<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <!-- First Blog Post -->
            <?php
            foreach ($postagens as $post) {
                $imagem;
                if ($post->img == 1) {
                    $imagem = "./assets/imgs/publicacoes/" . md5($post->id) . '.jpg';
                } else {
                    $imagem = "./assets/imgs/publicacoes/pubSemFoto.png";
                }
                ?>
                <h1><?php echo $post->titulo ?></h1>
                por <a href="<?php echo base_url('autor/' . $post->idautor . '/' . limpar($post->nome)) ?>"><?php echo $post->nome ?></a>
                <p class="lead">
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Postado em <?php echo postadoem($post->data) ?></p>
                <hr>
                <p><b><?php echo $post->subtitulo ?></b></p>
                <img class="img-responsive" src="<?= base_url($imagem) ?>" alt="">
                <hr>
                <p><?php echo $post->conteudo ?></p>
                <hr>
                <?php
            }
            ?>
        </div>